﻿using System;
using UnityEditor;
using UnityEngine;
using Level6Rogue.Utils;

namespace Level6Rogue.UnityShowcaseTemplate
{
    [CustomEditor(typeof(Showcase), true, isFallback = true)]
    public class ShowcaseEditor : Editor
    {
        private Showcase Target;
        
        private void Awake()
        {
            Target = target as Showcase;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.Space(5);
            
            using (new ColorBlock(Target.IsActive ? Color.green : Color.white))
            using (new EnableBlock(!Target.IsActive))
            if (GUILayout.Button(Target.IsActive ? "Active" : "Activate"))
            {
                Target.SetActive();
            }
        }
    }
}