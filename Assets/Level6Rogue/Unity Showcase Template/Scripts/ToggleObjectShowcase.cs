﻿using UnityEngine;
using System.Collections.Generic;
using Level6Rogue.Utils;

namespace Level6Rogue.UnityShowcaseTemplate
{
    [System.Serializable]
    public class ToggleObjectShowcase : Showcase
    {
        //Object to be toggled by this showcase.
        public GameObject ToggleObject;
        
        protected override void OnGUI()
        {
            base.OnGUI();

            if (IsActive && GUI.Button(new Rect(10, 130, 50, 30), "Toggle"))
            {
                ToggleObject.SetActive(!ToggleObject.activeSelf);
            }
        }
    }
}

