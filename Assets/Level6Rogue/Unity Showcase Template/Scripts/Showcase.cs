﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using Level6Rogue.Utils;

namespace Level6Rogue.UnityShowcaseTemplate
{
    [System.Serializable]
    public class Showcase : MonoBehaviour
    {
        public const string PREFS_KEY = "SHOWCASE_PREFS_KEY";

        [SerializeField] public string showcaseTitle = "Showcase";
        [TextArea] public string showcaseDesctription = "";
        
        public Showcase previousShowcase;
        public Showcase nextShowcase;

        [Space] public bool activateOnStart = false;

        private void Start()
        {
            if (activateOnStart)
                SetActive();
        }

        /// <summary>
        /// Returns true if this showcase is active.
        /// Uses GameObject.GetInstanceID() which is a non persistant value.
        /// </summary>
        public bool IsActive
        {
            get { return (GetInstanceID() == PlayerPrefs.GetInt(PREFS_KEY)); }
        }
        
        /// <summary>
        /// Sets this showcase to be the active showcase.
        /// </summary>
        public void SetActive()
        {
            PlayerPrefs.SetInt(PREFS_KEY, GetInstanceID());
        }
    
        protected virtual void FixedUpdate()
        {
            //If the Main Camera exists and this is the active point.
            if (Camera.main && IsActive)
            {
                //Move the camera to this point.
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, this.transform.position, Time.deltaTime * 6);
                
                //And Rotate to this points rotation.
                Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, this.transform.rotation, Time.deltaTime * 6);
            }
        }
    
        protected virtual void OnValidate()
        {
            //Updates references.
            
            if (previousShowcase)
                previousShowcase.nextShowcase = this;
            
            if (nextShowcase)
                nextShowcase.previousShowcase = this;
        }
    
        protected virtual void OnDrawGizmos()
        {
            Gizmos.color = IsActive ? Color.green : Color.gray;
            Gizmos.DrawWireSphere(transform.position, 0.3f);
    
            if (IsActive)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawLine(transform.position, transform.TransformPoint(Vector3.forward * 1));
            }
            
            if (previousShowcase)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.TransformPoint(Vector3.up * 0.02f), previousShowcase.transform.TransformPoint(Vector3.up * 0.02f));
            }
            
            if (nextShowcase)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(transform.position, nextShowcase.transform.position);
            }
        }
          
        protected virtual void OnGUI()
        {
            //If this is the active showcase.
            if (IsActive)
            {
                //Draw the 'previous' button.
                using (new EnableBlock(previousShowcase))
                if (GUI.Button(new Rect(10, 10, 18, 100), "<"))
                {
                    //Set the previous showcase active.
                    previousShowcase.SetActive();
                }
                
                //Draw the 'next' button.
                using (new EnableBlock(nextShowcase))
                if (GUI.Button(new Rect(10 + 30, 10, 18, 100), ">"))
                {
                    //Set the next showcase active.
                    nextShowcase.SetActive();
                }
                
                //Draw the showcase title.
                GUI.Label(new Rect(75, 10, 500, 18), showcaseTitle);
                
                //Draw the showcase description.
                GUI.TextArea(new Rect(75, 10 + 30, 500, 500), showcaseDesctription, "");
            }
        }
    }
}

