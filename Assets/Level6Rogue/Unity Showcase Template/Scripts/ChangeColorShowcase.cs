﻿using UnityEngine;

namespace Level6Rogue.UnityShowcaseTemplate
{
    public class ChangeColorShowcase : Showcase
    {
        public Material material;
        
        protected override void OnGUI()
        {
            base.OnGUI();
            
            if (IsActive && GUI.Button(new Rect(10, 130, 60, 30), "Change"))
            {
                if (material)
                    material.SetColor("_Color", new Color(Random.value,Random.value,Random.value,Random.value));
            }
        }
    }
}