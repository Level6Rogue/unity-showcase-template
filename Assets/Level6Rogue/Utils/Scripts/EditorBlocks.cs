﻿using System;
using UnityEngine;

namespace Level6Rogue.Utils
{
    public class ColorBlock : IDisposable
    {
        private readonly Color color;
        
        public ColorBlock(Color color)
        {
            this.color = GUI.color;
            GUI.color = color;
        }

        public void Dispose()
        {
            GUI.color = this.color;
        } 
    }
    
    public class EnableBlock : IDisposable
    {
        private readonly bool enabled;
        
        public EnableBlock(bool enabled)
        {
            this.enabled = GUI.enabled;
            GUI.enabled = enabled;
        }

        public void Dispose()
        {
            GUI.enabled = this.enabled;
        }
    }
    
    #if UNITY_EDITOR
    
    #endif
}